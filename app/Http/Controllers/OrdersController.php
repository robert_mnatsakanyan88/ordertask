<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Inventory;
use App\Models\Daily_discounts;
use App\Models\Orders;
use App\Models\User;
use App\Models\Digital_receipts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class OrdersController extends Controller
{
     /**
     * This function creates an order
     *
     *  
    */

    public function make_order(Request $request)
    {
        $user = User::find(Auth::id());
        $order = Orders::create([
            'user_id' => Auth::user()->id,
            'products'=> $request->product,
            'total_amount' => $request->total_amount,
        ]);
        
        return response([
            'message' =>  'Order was sucessfully placed',
        ], 200);

    }
}
