<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'products',
        'product_quantity',
        'discount_type',
        'total_amount'
    ];

    public function orders(){
        return $this->belongsTo('App\Models\User');
    }
}
